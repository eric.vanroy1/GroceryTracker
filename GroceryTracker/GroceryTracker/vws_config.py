from django.shortcuts import render, redirect

from ._cls_store import Store
from .frm_config import frm_add_store, frm_edit_store
from .helper import create_table, create_inv_table, get_records_all, get_records_partial
from django.conf import settings


def root(request):

    context = {
               'pageName': 'Purchase Root'
               }
    return render(request, 'form/datalist.html', context)


# STORES SECTION
def list_store(request):
    store_list = get_records_all(Store.getall_fordisplay_query())
    table = create_table(store_list, "List of Store", ('Store ID','Store Name', "Links"),'/config/editstore?storeid=', 'id_store')

    context = {'buttons':'<a href="/config/addstore" class="btn btn-primary">Add Store</a>',
        'pageName': 'Stores',
               'results': table,
               'environment': settings.ENVIRONMENT
               }
    return render(request, 'form/datalist.html', context)

def add_store(request):
    errormessage = ''
    if request.method == 'POST':
        if request.POST.get('cancel'):
                return redirect('/config/store')
        elif request.POST.get('Save'):
            newbuilding = Store.asnew(request.POST['storename'])
            if not newbuilding.checkexist():
                newbuilding.save()
                form = frm_add_store()
            else:
                errormessage = "Store Name Already Exists"
                form = frm_add_store(request.POST)
        else:
            errormessage = "Incorrect Action requested"
            form = frm_add_store(request.POST)
    else:
        form = frm_add_store()
    buildings_list = get_records_all(Store.getall_fordisplay_query())
    table = create_table(buildings_list, "List of Stores", ('Store ID','Store Name'))

    context = {'form': form,
               'pageName': 'Add Store',
               'results': table,
               'errmsg': errormessage,
               'environment': settings.ENVIRONMENT
               }
    return render(request, 'form/form.html', context)

def edit_store(request):
    errormessage = ''
    if request.method == 'POST':
        if request.POST.get('cancel'):
                return redirect('/config/store')
        elif request.POST.get('Save'):
            updatestore = Store.asnew(request.POST['storename'])
            updatestore.storeid = int(request.POST['storeid'])

            if updatestore.checkexistbyid():
                updatestore.update()
                return redirect('/config/store')
            else:
                errormessage = "Store not found"
        elif request.POST.get('Delete'):
            updatestore = Store.fromid(request.POST.get('storeid'))
            if updatestore.checkexistbyid():
                try:
                    updatestore.delete()
                    return redirect('/config/store')
                except Exception as e:
                    errormessage = e
            else:
                errormessage = "Store can not be found"
        else:
            errormessage = "Incorrect Action requested"
        form = frm_edit_store(request.POST)
    else:
        form = frm_edit_store()
        if request.GET.get('storeid'):
            updatestore = Store.fromid(request.GET['storeid'])
            form['storeid'].initial = request.GET['storeid']
            form['storename'].initial = updatestore.storename
    context = {'form': form,
               'pageName': 'Store Edit',
               'errmsg': errormessage,
               'environment': settings.ENVIRONMENT
               }
    return render(request, 'form/form.html', context)
