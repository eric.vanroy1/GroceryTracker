import mysql.connector
from django import forms
from django.core.validators import FileExtensionValidator
from django.utils.dateparse import parse_datetime
import datetime
from django.templatetags.static import static
from django.conf import settings


css_default = {'class': 'col-md'}
textarea_default = {'rows': '3', 'class': 'col-md'}


def mysql_connection():
    mysql_config = {
        'user': 'remoteuser',
        'password': 'secur3P@ss',
        'host': '192.168.1.25',
        'database': settings.DBNAME,
        'consume_results': True
    }
    return mysql_config


def create_table(tbldata, tblname, colheaders, editlink='', editidfield='', detaillink='', detailidfield='' ):
    table = []
    table.append(['<h5>' + tblname + '</h5>'])
    table.append(['<table class="table-sm" width="100%">'])

    if len(tbldata) > 0:
        if colheaders:
            tblrow = []
            tblrow.append('<thead>')
            for tblcol in colheaders:
                tblrow.append('<th>' + str(tblcol) + '</th>')
            tblrow.append('</thead>')
            table.append(tblrow)
        for tblrec in tbldata:
            tblrow = []
            tblrow.append('<tr>')

            for tblcol in tblrec.values():
                tblrow.append('<td>' + str(tblcol) + '</td>')
            if editlink:
                tblrow.append('<td><a href=' + editlink + str(tblrec[editidfield]) + '><img src="{0}" width="10" height="10"></a>'.format(static('images/iconfinder_edit.png')))
            if detaillink:
                tblrow.append('<td><a href=' + detaillink + str(tblrec[detailidfield]) + '><img src="{0}" width="10" height="10"></a>'.format(static('images/details.jpeg')))
            tblrow.append('</tr>')
            table.append(tblrow)

    else:
        table.append({'<th>No results to display.</th>'})

    table.append(['</table><br/>'])
    return table

def create_inv_table(tbldata, tblname, colheaders, editlink='',movelink='', idfield='' ):
    table = []
    table.append(['<h5>' + tblname + '</h5>'])
    table.append(['<table class="table-sm" width="100%">'])

    if len(tbldata) > 0:
        if colheaders:
            tblrow = []
            tblrow.append('<thead>')
            for tblcol in colheaders:
                tblrow.append('<th>' + str(tblcol) + '</th>')
            tblrow.append('</thead>')
            table.append(tblrow)
        for tblrec in tbldata:
            tblrow = []
            tblrow.append('<tr>')
            tblrow.append('<td>' + str(tblrec.get('SKU')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('ItemName')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('ItemDesc')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('PurchaseDetails')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('StorageLocation')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('InvtypeName')) + '</td>')
            tblrow.append('<td>' + str(tblrec.get('ExtendedValues')) + '</td>')
            if editlink:
                tblrow.append('<td><a href=' + editlink + str(tblrec[idfield]) + '><img src="{0}" width="20" height="20"></a>'.format(static('images/iconfinder_edit.png')) )
            if movelink:
                tblrow.append('<td><a href=' + movelink + str(tblrec[idfield]) + '><img src="{0}" width="30" height="30"></a>'.format(static('images/icons8-shipping-64.png')) )
            tblrow.append('</tr>')
            table.append(tblrow)

    else:
        table.append({'<th>No results to display.</th>'})

    table.append(['</table><br/>'])
    return table

def create_rpt_table(tbldata, tblname, colheaders=[], returninfo=() ):
    table = []
    table.append(['<a href=' + returninfo[1] + '>' + returninfo[0] + '</a>'])
    table.append(['<h5>' + tblname + '</h5>'])
    table.append(['<table class="table-sm table-striped table-bordered" width="100%">'])

    if len(tbldata) > 0:
        if colheaders:
            tblrow = []
            tblrow.append('<thead>')
            for tblcol in colheaders:
                tblrow.append('<th width=' + str(tblcol[1]) + '>' + str(tblcol[0]) + '</th>')
            tblrow.append('</thead>')
            table.append(tblrow)
        for tblrec in tbldata:
            tblrow = []
            tblrow.append('<tr>')
            tblrow.append('<td></td>')
            for tblcol in tblrec.values():
                tblrow.append('<td>' + str(tblcol) + '</td>')
            tblrow.append('<td></td>')
            tblrow.append('</tr>')
            table.append(tblrow)

    else:
        table.append({'<th>No results to display.</th>'})

    table.append(['</table><br/>'])
    return table

def get_choices(data, indexfield, textfield):
    returnoptions = []
    for option in data:
        choiceval = str(option[indexfield])
        returnoptions.append((choiceval, option[textfield]))
    return returnoptions

def get_auction_dropdown_choices(data):
    returnoptions = []
    for option in data:
        choiceval = option['idSalesTransactions']
        choicetext = option['SaleDate'].strftime('%Y-%m-%d') + ':' + option['SellerName'] + '-' + option['InvoiceID']
        returnoptions.append((choiceval, choicetext))
    return returnoptions

def get_purchase_dropdown_choices(data):
    returnoptions = [{0,'None Selected'}]
    for option in data:
        choiceval = option['idPurchase']
        if option.get('LotNum'):
            choicetext = str(option['idPurchase']) + '-' + str(option['auctionid']) + ':' + option.get('LotNum') + '-' + option.get('Description')
        else:
            choicetext = str(option['auctionid']) + ':N/A-' + option.get('Description')
        returnoptions.append((choiceval, choicetext))
    return returnoptions

def get_location_dropdown_choices(data):
    returnoptions = []
    for option in data:
        choiceval = option['idLocationList']
        choicetext = str(option['BuildingName']) + ':' + option['LocationName']
        returnoptions.append((choiceval, choicetext))
    return returnoptions

def get_inventory_dropdown_choices(data):
    returnoptions = []
    for option in data:
        choiceval = option['idInventory']
        choicetext = str(option['SKU']) + ':' + option['ItemName']
        returnoptions.append((choiceval, choicetext))
    return returnoptions

def get_listing_dropdown_choices(data):
    returnoptions = []
    for option in data:
        choiceval = option['idListings']
        # choicetext = str(option['LocationName']) + ':'
        choicetext = str(option['LocationName']) + ':' + option['skus']
        returnoptions.append((choiceval, choicetext))
    return returnoptions


# fields for forms
def create_fldchar(self, fldname, fldlabel, attr={}, required=True, initial=None, max_length=200, validators=[],
                   disabled=False):
    if attr is None:
        attr = {}
    fldchar = forms.CharField(
        initial=initial,
        label=fldlabel,
        max_length=max_length,
        required=required,
        validators=validators,
        disabled=disabled,
        widget=forms.TextInput(attrs={**css_default, **attr}),
    )
    self.fields[fldname] = fldchar
    return self

def create_flddrpdown(self, fldname, fldlabel, choices, attr={}, required=False):
    flddrpdown = forms.ChoiceField(
        label=fldlabel,
        required=required,
        choices=choices,
        widget=forms.Select(attrs={**css_default, **attr})
    )
    self.fields[fldname] = flddrpdown
    return self

def create_flddate(self, fldname, fldlabel, mindate='1900-01-01', maxdate=datetime.date.today, initial=None, attr={},
                   required=True):
    flddate = forms.DateField(
        label=fldlabel,
        required=required,
        initial=initial,
        widget=forms.DateInput(attrs={**css_default, **{'type': 'date', 'min': mindate, 'max': maxdate}, **attr}),
    )
    self.fields[fldname] = flddate
    return self

def create_flddecimal(self, fldname, fldlabel, minval=0, maxval=999999999, decplaces=2, attr={}, required=True):
    flddecimal = forms.DecimalField(
        label=fldlabel,
        min_value=minval,
        max_value=maxval,
        decimal_places=decplaces,
        required=required,
        widget=forms.NumberInput(attrs={**css_default, **attr})
    )
    self.fields[fldname] = flddecimal
    return self

def create_fldboolean(self, fldname, fldlabel):
    fldbool = forms.BooleanField(
        label=fldlabel,
        required=False
    )
    self.fields[fldname] = fldbool
    return self

def create_fldtxtarea(self, fldname, fldlabel, attr={}, required=True):
    fldtxtarea = forms.CharField(
        label=fldlabel,
        required=required,
        widget=forms.Textarea(attrs={**textarea_default, **attr}),
    )
    self.fields[fldname] = fldtxtarea
    return self


def create_pictureupload(self, fldname, fldlabel, attr={}, required=True):
    fileselection = forms.FileField(
        label=fldlabel,
        widget=forms.FileInput(attrs={**attr})
    )
    self.fields[fldname] = fileselection
    return self


def format_display_date(dtedate):
    dtedate = parse_datetime(dtedate)
    retstring = dtedate.strftime('%Y-%m-%d')
    return retstring

def get_records_all(query):
        try:
            cnx = mysql.connector.connect(**mysql_connection())
            # print(cnx.sql_mode)
            # removed ONLY_FULL_GROUP_BY
            cnx.sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION"
            cursor = cnx.cursor(buffered=True, dictionary=True)
            try:
                cursor.execute(query)
                result = cursor.fetchall()
                cursor.close()
                cnx.close()
            except Exception as e:
                cursor.close()
                cnx.close()
                raise e
        except Exception as e:
            raise e
        return result


def get_records_partial(query, filters):
    try:
        cnx = mysql.connector.connect(**mysql_connection())
        cursor = cnx.cursor(buffered=True, dictionary=True)
        try:
            cursor.execute(query, filters)
            result = cursor.fetchall()
            cursor.close()
            cnx.close()
        except Exception as e:
            cursor.close()
            cnx.close()
            raise e
    except Exception as e:
        raise e
    return result


def save_item(query, values):
    try:
        cnx = mysql.connector.connect(**mysql_connection())
        cursor = cnx.cursor(buffered=True, dictionary=True)
        try:
            cursor.execute(query, values)
            newID = cursor.lastrowid
            cnx.commit()
            cursor.close()
            cnx.close()
            result = newID
        except Exception as e:
            cursor.close()
            cnx.close()
            raise e
    except Exception as e:
        raise e
    return result


