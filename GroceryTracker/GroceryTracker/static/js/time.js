//is there an easier way with jQuery since it is already loaded

function startTime() {
    var today = new Date();
    var M = today.getMonth() + 1;
    var D = today.getDate();
    var Y = today.getFullYear();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    
    M = checkDigits(M);
    D = checkDigits(D);
    h = checkDigits(h);
    m = checkDigits(m);
    s = checkDigits(s);

    document.getElementById('txtDateTime').innerHTML = M + "/" + D + "/" + Y + " " + h + ":" + m + ":" + s;
    document.getElementById('txtCopyYear').innerHTML = "Last Updated: " + Y;

    var t = setTimeout(startTime, 500);
}
function checkDigits(i) {
    if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
    return i;
}