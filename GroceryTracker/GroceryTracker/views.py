from django.shortcuts import render
from .forms import storage_locale
from django.conf import settings



def index(request):
    form = storage_locale()

    context = {
               'pageName': 'Application Root'
               }
    return render(request, 'standard/welcome.html', context)

def error(request):
    form = storage_locale()
    if request.GET.get('errornum'):
        ErrorDescription = request.GET['errornum']
    else:
        ErrorDescription = "Unknown Error"
    context = {'errmsg': ErrorDescription,
               'formDescription':[['There has been an error that needs to be resolved.']],
               'pageName': 'Error',
               'environment': settings.ENVIRONMENT
               }
    return render(request, 'form/datalist.html', context)
