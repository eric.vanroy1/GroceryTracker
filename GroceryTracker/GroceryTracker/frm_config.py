from crispy_forms.layout import Submit, Layout, Row, Column
from crispy_forms.bootstrap import Field
from django import forms
from crispy_forms.helper import FormHelper
from datetime import date

from ._cls_store import Store
from .helper import get_choices, create_fldchar, create_flddrpdown, create_flddate, create_flddecimal, \
    get_auction_dropdown_choices, get_purchase_dropdown_choices, get_location_dropdown_choices, create_fldboolean, \
    create_fldtxtarea, create_pictureupload, get_inventory_dropdown_choices
from .helper import get_records_all

btn_css_default = 'btn btn-dark'


class frm_add_store(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id_add_store'
        self.helper.form_method = 'post'
        self.helper.form_action = '/config/addstore'
        self.helper.add_input(Submit('Save', 'Save Store', css_class=btn_css_default))
        self.helper.add_input(Submit('cancel', 'Cancel', css_class=btn_css_default, formnovalidate='formnovalidate'))
        create_fldchar(self, 'storename', 'Store Name', max_length=45, attr={'autofocus': 'autofocus'})

class frm_edit_store(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id_edit_building'
        self.helper.form_method = 'post'
        self.helper.form_action = '/config/editstore'
        self.helper.add_input(Submit('Save', 'Save Store', css_class=btn_css_default))
        self.helper.add_input(Submit('Delete', 'Delete', css_class=btn_css_default))
        self.helper.add_input(Submit('cancel', 'Cancel', css_class=btn_css_default, formnovalidate='formnovalidate'))
        create_fldchar(self, 'storeid', 'Store ID Number (Internal Use)', attr={'readonly': 'True'})
        create_fldchar(self, 'storename', 'Building Name', max_length=45, attr={'autofocus': 'autofocus'})
