from .helper import save_item, get_records_partial

# Queries
get_store = 'SELECT id_store, store_name from stores'
get_store_display = 'SELECT id_store, store_name from stores order by id_store desc'
get_store_by_name = get_store + ' where store_name = %(storename)s'
get_store_by_id = get_store + ' where id_store = %(storeid)s'
save_store = 'Insert into stores (store_name) values (%(storename)s)'
update_store = 'Update stores Set store_name=%(storename)s where (id_store = %(storeid)s)'
delete_store = 'Delete from stores where (id_store = %(storeid)s)'


class Store:
    storeid = ''
    storename = ''


    def __init__(self, storename, storeid=0):
        self.storeid = storeid
        self.storename = storename

    @classmethod
    def fromid(cls, storeid):
        record_result = get_records_partial(get_store_by_id, {'storeid': storeid})
        if len(record_result) == 1:
            return(cls(record_result[0]['store_name'], storeid))
        else:
            raise ValueError("Problem Getting a single store by ID")

    @classmethod
    def asnew(cls, storename):
        return(cls(storename))


    def save(self):
        formvalues = {
            'storename': self.storename,
        }
        try:
            save_item(save_store, formvalues)
            return True
        except Exception as e:
            raise e

    def update(self):
        formvalues = {
            'storeid': self.storeid,
            'storename': self.storename,
        }
        try:
            save_item(update_store, formvalues)
            return True
        except Exception as e:
            raise e

    def delete(self):
        formvalues = {
            'storeid': self.storeid,
        }
        try:
            save_item(delete_store, formvalues)
            return True
        except Exception as e:
            if e.errno == 1451:
                raise Exception("Cannot Delete Building. There are Locations assigned to it.")
            else:
                raise e

    def checkexistbyid(self):
        record_result = get_records_partial(get_store_by_id, {'storeid' :self.storeid})
        if len(record_result) > 0:
            return True
        else:
            return False

    def checkexist(self):
        record_result = get_records_partial(get_store_by_name, {'storename' :self.storename})
        if len(record_result) > 0:
            return True
        else:
            return False

    @staticmethod
    def getall_query():
        return get_store

    @staticmethod
    def getall_fordisplay_query():
        return get_store_display






