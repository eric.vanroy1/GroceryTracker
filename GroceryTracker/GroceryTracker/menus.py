from django.conf import settings

MENUS = {
    'NAV_TOP': [
        {
            'name': 'Home',
            'url': '/',
        },
        {
            'name': 'Inventory Management',
            'url': '#',
            'submenu': [
                {
                    'name': 'Inventory Moves',
                    'url': '/purchases/moveinventory'
                }
            ]
        },
        {
            'name': 'Purchases',
            'url': '/purchases/',
            'submenu': [
                {
                    'name': 'Inventory',
                    'url': '/purchases/inventory'
                },
                {
                    'name': 'Purchases',
                    'url': '/purchases/purchases'
                },
                {
                    'name': 'Auctions',
                    'url': '/purchases/auctions'
                },
                {
                    'name': 'Sellers',
                    'url': '/purchases/sellers'
                }
            ]
        },
        {
            'name': 'Sales',
            'url': '/sales/',
            'submenu': [
                {
                    'name': 'Listings',
                    'url': '/sales/listing'
                },
                {
                    'name': 'Sales',
                    'url': '/sales/sale'
                },
                {
                    'name': 'Add Quick Sale',
                    'url': '/sales/quicksale'
                },
                {
                    'name': 'Listing Location',
                    'url': '/sales/listinglocation'
                },
                {
                    'name': 'Listing Status',
                    'url': '/sales/listingstatus'
                }

            ]

        },
        {
            'name': 'Configuration',
            'url':'/',
            'submenu': [
                {
                    'name': 'Inventory Categories',
                    'url': '/purchases/category'
                },
                {
                    'name': 'Storage Locations',
                    'url': '/purchases/locations'
                },
                {
                    'name': 'Storage Buildings',
                    'url': '/purchases/buildings'
                }
            ]
        },
        {
            'name': 'Reports',
            'url': '/reports/',
            'submenu': [
                {
                    'name': 'Inventory',
                    'url': '/reports/inventory'
                },
                {
                    'name': 'Inventory List',
                    'url': '/reports/inventorylist'
                },
                {
                    'name': 'In Transit List',
                    'url': '/reports/inventorylist_intransit'
                },
                {
                    'name': 'Inventory List by SKU',
                    'url': '/reports/inventorylist_by_id'
                },

                {
                    'name': 'Purchase',
                    'url': '/reports/purchase'
                }

            ]

        }
    ]
}
